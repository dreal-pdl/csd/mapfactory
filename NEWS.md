# mapfactory 0.0.0.9006  
* Introduction du parametre na_couleur dans la fonction creer_carte pour maîtriser la couleur des territoires dont l'indicateur cartographié vaut NA.

# mapfactory 0.0.0.9005

- Passage du caractère spécial séparateur de milliers et du % (ascii \u202f) en parametre par défaut des fonctions de formatage des nombre, seulement si la session est interactive (car sur OS linux, si non interactif, le device pdf est employé pour les sortie ggplot et il ne sait pas utiliser le caractère ascii \u202f en tant que séparateur de milliers dans les légendes. Message d'erreur constaté `Error in grid.Call(C_textBounds, as.graphicsAnnot(x$label), x$x, x$y,... conversion failure`) 

- Fix bug `creer_carte()` : la fonction échouait lorsque l'indicateur à cartographier s'appelait `valeur`

# mapfactory 0.0.0.9004
Passage du caractère séparateur de milliers et du % (ascii \u202f) en paramètre de `creer_carte()`, en raison de pb d'encodage sur certaines OS

# mapfactory 0.0.0.9003
* `fonds_carto()` : ajout du parametre `ombre` pour contrôler le décalage de l'ombre régionale, parfois trop écartée par exemple dans les DROM.
* bug fix `fond_carto()` _'x has a crs without units: can't convert units'_ : suppression de l'unité (mètres) du paramètre `espace` quand calculé par défaut, elle faisait planter le `filtrer_cog_geo()` sur certaines machines. 

# mapfactory 0.0.0.9002
* Factorisation des fonctions de création carte `creer_carte_epci()`, `creer_carte_epci_prop()`, `creer_carte_categorie_epci()`, `creer_carte_communes()`, `creer_carte_communes_prop()`, `creer_carte_categorie_communes()`, qui deviennent des cas particuliers d'usage de la nouvelle fonction `creer_carte()`.
* Implémentation des développements de {`propre.rpls`} :  
 - Possibilité d'introduire une distance autours du fonds_carto, 
 - Elargisement de l'emprise du fond carto des DROM dans les fonctions `creer_carte()`,
 - Possibilité de contrôler l'information de survol de la carte (popover)
 - Possibilité de faire des cartes régionales à la maille départementale
* Introduction du paramètre `na_label` aux fonctions de créations de carte pour contrôler comment les NA s'affichent dans la légende.

## Breaking change :
* le paramètre `suffixe` des fonctions de créations de carte, doit comprendre l'espace séparateur. L'espace ajouté par défaut a dû être supprimé pour le contrôle des NA. 

# mapfactory 0.0.0.9001
* Résolution de la dépendance orpheline à `santoku::lbl_format()`  

# mapfactory 0.0.0.9000

* Ajout des fonctions de création de carte : 

  - `creer_carte_epci()`
  - `creer_carte_epci_prop()`
  - `creer_carte_categorie_epci()`
  - `creer_carte_communes()`
  - `creer_carte_communes_prop()`
  - `creer_carte_categorie_communes()`

* Added a `NEWS.md` file to track changes to the package.
