#' fonction utilitaire de formatage en pourcentage pour le francais
#'
#' @description fonction utilitaire de formatage en pourcentage pour le francais

#' @param x un nombre à formater en pourcentage
#' @param dec un entier désignant le nombre de chiffres après la virgule souhaité (1 par défaut)
#' @param sep le caractere separateur des milliers et separant la valeur du caractere de pourcentage, ascii u202f par défaut.
#'
#' @return une chaîne de texte, x %, avec transformation de la décimale en virgule et insertion d'un espace insécable
#'
#' @importFrom attempt stop_if_not
#'
#' @export
#'
#' @examples
#' format_fr_pct(100/3)
format_fr_pct <- function(x, dec = 1, sep = NULL) {
  if(is.null(sep)) {
    if(interactive()) { #Sys.info()["sysname"]=="Windows"
      sep <- "\u202f"
    } else {
      sep <- " "
    }
  }
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_pct(x, dec)")
  paste0(formatC(x, decimal.mark = ",", big.mark = sep, format = "f", digits = dec), sep, "%")
}

#' fonction utilitaire de formatage en pourcentage pour le francais
#'
#' @description fonction utilitaire de formatage en pourcentage pour le francais

#' @param x un nombre à formater en pourcentage
#' @param accuracy la précision souhaitée
#'
#' @return une chaîne de texte, x %, avec transformation de la décimale en virgule et insertion d'un espace insécable
#'
#' @importFrom attempt stop_if_not
#' @importFrom scales label_percent
#' @export
#'
#' @examples
#' format_fr_pct_2(100/3,0.2)
format_fr_pct_2 <- function(x, accuracy=0.1) {
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_pct(x, dec)")
  french_percent <- scales::label_percent(
      decimal.mark = ",",
      suffix = " %",
      scale = 1,
      accuracy = accuracy
    )
  french_percent(x)
}
#' fonction utilitaire de formatage de nombre pour le francais
#'
#' @description fonction utilitaire de formatage de nombre pour le francais

#' @param x un nombre à formater en français
#' @param dec un entier désignant le nombre de chiffres après la virgule souhaité (1 par défaut)
#' @param big_mark le separateur des milliers
#' @return une chaîne de texte avec transformation de la décimale en virgule et insertion d'un caractère spécifié via big_mark au niveau du séparateur de milliers
#'
#' @importFrom attempt stop_if_not
#'
#' @export
#'
#' @examples
#' format_fr_nb(10000000/7)
#'

#' @importFrom attempt stop_if_not

format_fr_nb <- function(x, dec = 1, big_mark = NULL) {
  if(is.null(big_mark)) {
    if(interactive()) { #Sys.info()["sysname"]=="Windows"
      big_mark <- "\u202f"
    } else {
      big_mark <- " "
    }

  }
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_pct(x, dec)")
  paste0(formatC(x, decimal.mark = ",", format = "f", digits = dec, big.mark = big_mark))
}

#' fonction utilitaire de formatage de nombre pour le francais
#'
#' @param x un nombre à formater en français
#' @param dec un entier désignant le nombre de chiffres après la virgule souhaité (1 par défaut)
#' @param big_mark le caractere separateur des milliers et separant la valeur du %, ascii u202f par défaut.
#' @param pourcent TRUE pour un formatage en pourcentage
#'
#' @return une chaîne de texte avec transformation de la décimale en virgule et insertion d'un caractère spécifié via big_mark au niveau du séparateur de milliers
#' @importFrom attempt stop_if_not
#' @export
#'
#' @examples
#' format_fr(10000000/7)
format_fr <- function(x, dec = 1, big_mark = NULL, pourcent = FALSE) {
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_pct(x, dec)")
  if (pourcent) {
    format_fr_pct(x, dec = dec, sep = big_mark)
  }
  else(
    format_fr_nb(x, dec = dec, big_mark = big_mark)

  )

}
