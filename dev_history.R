usethis::create_package("mapfactory")
usethis::use_git()

usethis::use_git_remote(url = "https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/mapfactory.git")
gert::git_push(set_upstream = TRUE)
usethis::edit_file("dev_history.R")
usethis::use_build_ignore('dev_history.R')
usethis::use_gpl3_license()
usethis::edit_file('DESCRIPTION')
usethis::use_pipe()
usethis::use_r("get_id_reg")
usethis::use_r("fonds_carto")
usethis::use_r("globals")


usethis::use_r("utils")
usethis::use_test("utils")

#discretisation
usethis::use_r("discretisation")
usethis::use_test("discretisation")


# cartes
usethis::use_r("creer_carte")
usethis::use_test("creer_carte")

# readme

usethis::use_readme_rmd()

# news

usethis::use_news_md()

# Vignette
usethis::use_vignette(name = "creer_carte",title = "creer des cartes avec mapfactory")

usethis::use_gitlab_ci()
# pkgdown

usethis::use_pkgdown()

# lifecycle

usethis::use_lifecycle_badge("experimental")
#----
attachment::att_amend_desc()
devtools::document()
devtools::check()
devtools::install()
